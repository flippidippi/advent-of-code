export type Puzzle = () => Promise<string>

/*
 * Starts puzzles for the day and logs outputs
 */
async function start (year: string, day: string, startPart1: Puzzle, startPart2: Puzzle): Promise<void> {
  console.log()
  console.log(`-----Day ${year}-${day}-----`)
  console.log('Part 1: ', await startPart1())
  console.log('Part 2: ', await startPart2())
  console.log()
}

export {
  start
}
