import { start } from './puzzle'

jest.spyOn(console, 'log').mockImplementation(() => { /* intentionally blank */ })

describe('puzzle', () => {
  it('should call puzzles', async () => {
    const startPart1 = jest.fn()
    const startPart2 = jest.fn()
    await start('2021', '01', startPart1, startPart2)
    expect(startPart1).toHaveBeenCalled()
    expect(startPart1).toHaveBeenCalled()
  })
})
