import { createInterface, Interface } from 'readline'
import { createReadStream } from 'fs'
import { join } from 'path'

export type Converter<T> = (line: string) => T | null | undefined
export type ConverterType<T extends Converter<any>> = T extends ((line: string) => infer R) ? R : never
export type ConverterTypes<T extends Converter<any>> = Array<NonNullable<ConverterType<T>>>

/*
 * Gets lines of asset given
 */
function getInputLines (dirname: string, file: string): Interface {
  return createInterface({
    input: createReadStream(
      join(dirname, process.env.NODE_ENV === 'test' ? '..' : '', 'assets', file)
    )
  })
}

/*
 * Get lines of asset given as an array with converter to parse each line
 * Return null or undefined from the converter to skip the current line
 */
async function getInputArray<T extends Converter<any>> (
  dirname: string,
  file: string,
  converter: T
): Promise<ConverterTypes<T>> {
  const input = []
  const lines = getInputLines(dirname, file)

  for await (const line of lines) {
    const converted = converter(line)
    if (converted != null) {
      input.push(converter(line))
    }
  }

  return input as ConverterTypes<T>
}

export {
  getInputLines,
  getInputArray
}
