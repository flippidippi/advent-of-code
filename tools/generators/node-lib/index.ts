import { libraryGenerator } from '@nrwl/node'
import {
  Tree,
  readProjectConfiguration,
  names,
  updateProjectConfiguration,
  readJson,
  writeJson,
  ProjectConfiguration,
  NxJsonProjectConfiguration,
  generateFiles,
  joinPathFragments,
  getWorkspaceLayout,
  offsetFromRoot as getOffsetFromRoot
} from '@nrwl/devkit'

export interface GeneratorOptions {
  name: string
  directory?: string
  tags?: string
}

/*
 * Fix project configuration
 */
function fixProjectConfiguration (tree: Tree, projectName: string, projectConfiguration: ProjectConfiguration & NxJsonProjectConfiguration): void {
  // Fix lint file patterns
  if (projectConfiguration.targets?.lint.options?.lintFilePatterns != null) {
    projectConfiguration.targets.lint.options.lintFilePatterns = [`${projectConfiguration.root}/**/*.{ts,tsx,js,jsx}`]
  }

  updateProjectConfiguration(tree, projectName, projectConfiguration)
}

/*
 * Fix .eslintrc.json
 */
function fixEslintrc (tree: Tree, projectConfiguration: ProjectConfiguration & NxJsonProjectConfiguration): void {
  const eslintrcPath = `${projectConfiguration.root}/.eslintrc.json`
  const eslintrc = readJson(tree, eslintrcPath)

  eslintrc.overrides[1].parserOptions = {
    project: `${projectConfiguration.root}/tsconfig.*?.json`
  }

  writeJson(tree, eslintrcPath, eslintrc)
}

/*
 * Nx generator to generate new node libs
 */
async function generator (tree: Tree, schema: GeneratorOptions): Promise<() => void> {
  // Use default node library generator
  await libraryGenerator(tree, {
    name: schema.name,
    directory: schema.directory,
    tags: schema.tags,
    simpleModuleName: true
  })

  // Get project names
  const { libsDir } = getWorkspaceLayout(tree)
  const { name, propertyName, fileName } = names(schema.name)
  const projectDirectory = schema.directory != null
    ? `${names(schema.directory).fileName}/${fileName}`
    : fileName
  const projectName = projectDirectory.replace(/\//g, '-')
  const projectRoot = joinPathFragments(libsDir, projectDirectory)
  const offsetFromRoot = getOffsetFromRoot(projectRoot)

  // Get project configuration
  const projectConfiguration = readProjectConfiguration(tree, projectName)

  // Run fixes
  fixProjectConfiguration(tree, projectName, projectConfiguration)
  fixEslintrc(tree, projectConfiguration)

  // Generate files
  generateFiles(tree, joinPathFragments(__dirname, './files'), projectConfiguration.root, {
    name,
    propertyName,
    fileName,
    projectRoot,
    projectName,
    offsetFromRoot,
    tmpl: ''
  })

  return () => { /* intentionally blank */ }
}

export default generator
