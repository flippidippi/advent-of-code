import { applicationGenerator } from '@nrwl/node'
import {
  Tree,
  readProjectConfiguration,
  names,
  updateProjectConfiguration,
  readJson,
  writeJson,
  ProjectConfiguration,
  NxJsonProjectConfiguration,
  generateFiles,
  joinPathFragments,
  getWorkspaceLayout,
  offsetFromRoot as getOffsetFromRoot
} from '@nrwl/devkit'

export interface GeneratorOptions {
  year: string
  day: string
}

/*
 * Fix project configuration
 */
function fixProjectConfiguration (tree: Tree, projectName: string, projectConfiguration: ProjectConfiguration & NxJsonProjectConfiguration): void {
  // Fix lint file patterns
  if (projectConfiguration.targets?.lint.options?.lintFilePatterns != null) {
    projectConfiguration.targets.lint.options.lintFilePatterns = [`${projectConfiguration.root}/**/*.{ts,tsx,js,jsx}`]
  }

  // Remove configurations
  if (projectConfiguration?.targets?.build.configurations != null) {
    projectConfiguration.targets.build.configurations = undefined
  }

  updateProjectConfiguration(tree, projectName, projectConfiguration)
}

/*
 * Fix .eslintrc.json
 */
function fixEslintrc (tree: Tree, projectConfiguration: ProjectConfiguration & NxJsonProjectConfiguration): void {
  const eslintrcPath = `${projectConfiguration.root}/.eslintrc.json`
  const eslintrc = readJson(tree, eslintrcPath)

  eslintrc.overrides[1].parserOptions = {
    project: `${projectConfiguration.root}/tsconfig.*?.json`
  }

  writeJson(tree, eslintrcPath, eslintrc)
}

/*
 * Nx generator to generate new node libs
 */
async function generator (tree: Tree, schema: GeneratorOptions): Promise<() => void> {
  // Use default node application generator
  const inputName = schema.day
  const inputDirectory = `puzzle/${schema.year}`
  await applicationGenerator(tree, {
    name: inputName,
    directory: inputDirectory,
    tags: 'scope:puzzle'
  })

  // Get project names
  const { appsDir } = getWorkspaceLayout(tree)
  const { name, propertyName, fileName } = names(inputName)
  const projectDirectory = `${names(inputDirectory).fileName}/${fileName}`
  const projectName = projectDirectory.replace(/\//g, '-')
  const projectRoot = joinPathFragments(appsDir, projectDirectory)
  const offsetFromRoot = getOffsetFromRoot(projectRoot)

  // // Get project configuration
  const projectConfiguration = readProjectConfiguration(tree, projectName)

  // // Run fixes
  fixProjectConfiguration(tree, projectName, projectConfiguration)
  fixEslintrc(tree, projectConfiguration)

  // Generate files
  generateFiles(tree, joinPathFragments(__dirname, './files'), projectConfiguration.root, {
    year: schema.year,
    day: schema.day,
    name,
    propertyName,
    fileName,
    projectRoot,
    projectName,
    offsetFromRoot,
    tmpl: ''
  })

  // Delete unneeded files
  tree.delete(joinPathFragments(projectRoot, 'src/environments'))
  tree.delete(joinPathFragments(projectRoot, 'src/app/.gitkeep'))

  return () => { /* intentionally blank */ }
}

export default generator
