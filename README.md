# advent-of-code
Personal solutions for https://adventofcode.com

## Development

### Run
- Install dependencies with `npm ci`
- Run a puzzle with `npm start puzzle-YYYY-MM:serve`
- Generate a new puzzle with `npm run workspace-generator puzzle`

### Edit
- Puzzles are in [`apps/puzzle`](apps/puzzle)
- Each puzzle contains the solutions to part 1 and 2 in their respective `src/app` along with test files

### Test
- Test a puzzle with `npm start puzzle-YYYY-MM:test`
- Use `npm start puzzle-YYYY-MM:test -- --watch` to watch for changes
  - This is great for solving examples and refactoring
