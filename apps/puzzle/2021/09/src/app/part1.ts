import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  HeightsConverter,
  HeightsConverterTypes
} from './converter'

/*
 * Calculates if the point given is lower than the point at row, col
 * 1 for true or the other point is undefined
 * 0 for false
 */
function calcIsLower (heights: HeightsConverterTypes, point: number, row: number, col: number): number {
  if (heights[row]?.[col] != null) {
    if (point < heights[row][col]) {
      return 1
    }
  } else {
    return 1
  }

  return 0
}

/*
 * Calculates the risk level of low point
 */
function calcLowPointRiskLevel (heights: HeightsConverterTypes, row: number, col: number): number {
  const point = heights[row][col]
  let lower = 0

  lower += calcIsLower(heights, point, row + 1, col)
  lower += calcIsLower(heights, point, row - 1, col)
  lower += calcIsLower(heights, point, row, col + 1)
  lower += calcIsLower(heights, point, row, col - 1)

  return lower === 4 ? heights[row][col] + 1 : 0
}

/*
 * Calculates the risk level of low points
 */
function calcLowPointsRiskLevel (heights: HeightsConverterTypes): number {
  let riskLevel = 0

  for (let r = 0; r < heights.length; r++) {
    for (let c = 0; c < heights[0].length; c++) {
      riskLevel += calcLowPointRiskLevel(heights, r, c)
    }
  }

  return riskLevel
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const heights = await getInputArray<HeightsConverter>(__dirname, 'heights.txt', converter)

  return calcLowPointsRiskLevel(heights).toString()
}

export {
  start
}
