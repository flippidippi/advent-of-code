import { Converter, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export type HeightsConverter = Converter<number[]>
export type HeightsConverterTypes = ConverterTypes<HeightsConverter>

/*
 * Converts input
 */
const converter: HeightsConverter = line => {
  return line.split('').map(h => +h)
}

export { converter }
