import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  HeightsConverter,
  HeightsConverterTypes
} from './converter'

/*
 * Calculates the nested basin size
 */
function calcLowerBasin (heights: HeightsConverterTypes, row: number, col: number): number {
  if (heights[row]?.[col] != null) {
    if (heights[row][col] !== 9) {
      heights[row][col] = 9
      return calcBasinSize(heights, row, col) + 1
    }
  }

  return 0
}

/*
 * Calculates the basin size given a valid starting point
 */
function calcBasinSize (heights: HeightsConverterTypes, row: number, col: number): number {
  const point = heights[row][col]
  let total = point === 9 ? 0 : 1
  heights[row][col] = 9

  total += calcLowerBasin(heights, row + 1, col)
  total += calcLowerBasin(heights, row - 1, col)
  total += calcLowerBasin(heights, row, col + 1)
  total += calcLowerBasin(heights, row, col - 1)

  return total
}

/*
 * Calculates the risk level of low points
 */
function calcLargestBasins (heights: HeightsConverterTypes): number {
  const basins: number[] = []

  for (let r = 0; r < heights.length; r++) {
    for (let c = 0; c < heights[0].length; c++) {
      if (heights[r][c] !== 9) {
        basins.push(calcBasinSize(heights, r, c))
      }
    }
  }

  return basins.sort((a, b) => b - a).slice(0, 3).reduce((t, b) => t * b, 1)
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const heights = await getInputArray<HeightsConverter>(__dirname, 'heights.txt', converter)

  return calcLargestBasins(heights).toString()
}

export {
  start
}
