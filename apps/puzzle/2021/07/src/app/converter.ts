import { Converter, ConverterType } from '@advent-of-code/puzzle/util/core'

export type CrabsConverter = Converter<number[]>
export type CrabsConverterType = NonNullable<ConverterType<CrabsConverter>>

/*
 * Converts input
 */
const converter: CrabsConverter = line => {
  return line.split(',').map(c => +c)
}

export { converter }
