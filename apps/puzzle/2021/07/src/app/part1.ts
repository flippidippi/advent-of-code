import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  CrabsConverter,
  CrabsConverterType
} from './converter'

/*
 * Calculates crab fuel cost
 */
function calcFuelCost (crabs: CrabsConverterType, median: number): number {
  return crabs.reduce((t, c) => t + Math.abs(median - c), 0)
}

/*
 * Calculates minimum crab fuel cost
 */
function calcMinFuelCost (crabs: CrabsConverterType): number {
  const crabsSort = crabs.sort((a, b) => a - b)
  const half = crabs.length / 2

  if (!Number.isInteger(half)) {
    return calcFuelCost(crabsSort, crabsSort[Math.floor(half)])
  } else {
    return Math.min(
      calcFuelCost(crabsSort, crabsSort[half - 1]),
      calcFuelCost(crabsSort, crabsSort[half])
    )
  }
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const [crabs] = await getInputArray<CrabsConverter>(__dirname, 'crabs.txt', converter)

  return calcMinFuelCost(crabs).toString()
}

export {
  start
}
