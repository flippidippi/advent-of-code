import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  CrabsConverter,
  CrabsConverterType
} from './converter'

/*
 * Calculates crab fuel cost
 */
function calcFuelCost (crabs: CrabsConverterType, mean: number): number {
  return crabs.reduce((t, c) => {
    const diff = Math.abs(mean - c)
    return t + ((diff * (diff + 1)) / 2)
  }, 0)
}

/*
 * Calculates minimum crab fuel cost
 */
function calcMinFuelCost (crabs: CrabsConverterType): number {
  const mean = crabs.reduce((t, c) => t + c, 0) / crabs.length

  if (Number.isInteger(mean)) {
    return calcFuelCost(crabs, mean)
  } else {
    return Math.min(
      calcFuelCost(crabs, Math.floor(mean)),
      calcFuelCost(crabs, Math.ceil(mean))
    )
  }
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const [crabs] = await getInputArray<CrabsConverter>(__dirname, 'crabs.txt', converter)

  return calcMinFuelCost(crabs).toString()
}

export {
  start
}
