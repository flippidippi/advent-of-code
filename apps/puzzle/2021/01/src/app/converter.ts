import { Converter, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export type DepthConverter = Converter<number>
export type DepthConverterTypes = ConverterTypes<DepthConverter>

/*
 * Converts input
 */
const converter: DepthConverter = line => +line

export { converter }
