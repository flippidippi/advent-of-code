import { getInputArray } from '@advent-of-code/puzzle/util/core'
import { converter, DepthConverter, DepthConverterTypes } from './converter'

/*
 * Calculates number of increases in array
 */
function calcDepthIncreases (depths: DepthConverterTypes): number {
  let increases = 0

  for (let i = 0; i < depths.length - 1; i++) {
    if (depths[i] < depths[i + 1]) {
      increases++
    }
  }

  return increases
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const depths = await getInputArray<DepthConverter>(__dirname, 'depths.txt', converter)

  return calcDepthIncreases(depths).toString()
}

export { start }
