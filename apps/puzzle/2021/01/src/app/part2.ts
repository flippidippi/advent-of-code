import { getInputArray } from '@advent-of-code/puzzle/util/core'
import { converter, DepthConverter, DepthConverterTypes } from './converter'

/*
 * Calculates number of increases in array
 */
function calcDepthIncreases (depths: DepthConverterTypes): number {
  let increases = 0

  for (let i = 0; i < depths.length - 3; i++) {
    const window1 = depths[i] + depths[i + 1] + depths[i + 2]
    const window2 = depths[i + 1] + depths[i + 2] + depths[i + 3]

    if (window1 < window2) {
      increases++
    }
  }

  return increases
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const depths = await getInputArray<DepthConverter>(__dirname, 'depths.txt', converter)

  return calcDepthIncreases(depths).toString()
}

export { start }
