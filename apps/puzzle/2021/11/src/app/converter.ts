import { Converter, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export type EnergiesConverter = Converter<number[]>
export type EnergiesConverterTypes = ConverterTypes<EnergiesConverter>

/*
 * Converts input
 */
const converter: EnergiesConverter = line => {
  return line.split('').map(e => +e)
}

export { converter }
