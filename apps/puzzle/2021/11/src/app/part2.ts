import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  EnergiesConverter,
  EnergiesConverterTypes
} from './converter'

/*
 * Increases point energy after adjacent flash and returns resulting flashes if any
 */
function increaseFlash (energies: EnergiesConverterTypes, row: number, col: number): number {
  const point = energies[row]?.[col]

  if (point != null && point < 10) {
    energies[row][col] += 1
    return calcFlash(energies, row, col)
  }

  return 0
}

/*
 * Calculates flash total for point
 */
function calcFlash (energies: EnergiesConverterTypes, row: number, col: number): number {
  let flashes = 0

  if (energies[row][col] === 10) {
    flashes += 1
    energies[row][col] += 1
    flashes += increaseFlash(energies, row - 1, col - 1)
    flashes += increaseFlash(energies, row - 1, col)
    flashes += increaseFlash(energies, row - 1, col + 1)
    flashes += increaseFlash(energies, row, col - 1)
    flashes += increaseFlash(energies, row, col + 1)
    flashes += increaseFlash(energies, row + 1, col - 1)
    flashes += increaseFlash(energies, row + 1, col)
    flashes += increaseFlash(energies, row + 1, col + 1)
  }

  return flashes
}

/*
 * Calculates the first step that all ocotpus flash simultaneously
 */
function calcFirstSimultaneousFlash (energies: EnergiesConverterTypes): number {
  const flashGoal = energies.length * energies[0].length
  let step = 0
  let flashes = 0

  while (flashes !== flashGoal) {
    flashes = 0
    step += 1

    for (let r = 0; r < energies.length; r++) {
      for (let c = 0; c < energies[0].length; c++) {
        energies[r][c] += 1
      }
    }

    for (let r = 0; r < energies.length; r++) {
      for (let c = 0; c < energies[0].length; c++) {
        flashes += calcFlash(energies, r, c)
      }
    }

    for (let r = 0; r < energies.length; r++) {
      for (let c = 0; c < energies[0].length; c++) {
        if (energies[r][c] > 9) {
          energies[r][c] = 0
        }
      }
    }
  }

  return step
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const energies = await getInputArray<EnergiesConverter>(__dirname, 'energies.txt', converter)

  return calcFirstSimultaneousFlash(energies).toString()
}

export {
  start
}
