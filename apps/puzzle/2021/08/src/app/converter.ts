import { Converter, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export type Number = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
export type Signal = 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g'
export type Pattern = Signal[]
export type Patterns = Pattern[]
export interface Entry {
  input: Patterns
  output: Patterns
}
export type EntriesConverter = Converter<Entry>
export type EntriesConverterTypes = ConverterTypes<EntriesConverter>
export type InputPatterns = { [k in Number]: Pattern | null }

/*
 * Converts input
 */
const converter: EntriesConverter = line => {
  const [input, output] = line.split(' | ').map(p => p.split(' ').map(s => s.split('') as Pattern))

  return { input, output }
}

export { converter }
