import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  EntriesConverter,
  EntriesConverterTypes,
  Number,
  Entry,
  InputPatterns
} from './converter'

// Length to unique patterns
const UNIQUE_PATTERNS: { [k: number]: Number } = { 2: 1, 4: 4, 3: 7, 7: 8 }

/*
 * Calculates the entry output
 */
function calcEntryOutput (entry: Entry): number {
  const inputs: InputPatterns = { 0: null, 1: null, 2: null, 3: null, 4: null, 5: null, 6: null, 7: null, 8: null, 9: null }

  // Find 1, 4, 7, 8
  for (const pattern of entry.input) {
    const segment = UNIQUE_PATTERNS[pattern.length]
    if (segment != null) {
      inputs[segment] = pattern
    }
  }

  // Find 5
  for (const pattern of entry.input) {
    if (pattern.length === 5 && pattern.filter(p => inputs[7]?.includes(p)).length === 3) {
      inputs[3] = pattern
    }
  }

  // Find 9
  for (const pattern of entry.input) {
    if (!Object.values(inputs).includes(pattern) && pattern.filter(p => inputs[4]?.includes(p)).length === 4) {
      inputs[9] = pattern
    }
  }

  // Find 5, 2
  for (const pattern of entry.input) {
    if (!Object.values(inputs).includes(pattern) && pattern.length === 5) {
      const same = pattern.filter(p => inputs[9]?.includes(p)).length
      if (same === 5) {
        inputs[5] = pattern
      } else {
        inputs[2] = pattern
      }
    }
  }

  // Find 0, 6
  for (const pattern of entry.input) {
    if (!Object.values(inputs).includes(pattern)) {
      const same = pattern.filter(p => inputs[1]?.includes(p)).length
      if (same === 2) {
        inputs[0] = pattern
      } else {
        inputs[6] = pattern
      }
    }
  }

  // Find result
  let result = ''
  for (const outPattern of entry.output) {
    for (const [digit, inPattern] of Object.entries(inputs)) {
      if (outPattern.length === inPattern?.length && outPattern.every(p => inPattern?.includes(p))) {
        result = `${result}${digit}`
      }
    }
  }

  return +result
}

/*
 * Calculates the output of each entry
 */
function calcEntriesOutput (entries: EntriesConverterTypes): number {
  return entries.reduce((t, e) => t + calcEntryOutput(e), 0)
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const entries = await getInputArray<EntriesConverter>(__dirname, 'entries.txt', converter)

  return calcEntriesOutput(entries).toString()
}

export {
  start
}
