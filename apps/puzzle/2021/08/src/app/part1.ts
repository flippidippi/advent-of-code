import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  EntriesConverter,
  EntriesConverterTypes
} from './converter'

// Number of segments that are unique (1, 4, 7, 8)
const UNIQUE_SEGMENTS = [2, 4, 3, 7]

/*
 * Calculates the number of unique segments
 */
function calcUniqueSegments (entries: EntriesConverterTypes): number {
  let unique = 0

  for (const entry of entries) {
    for (const pattern of entry.output) {
      if (UNIQUE_SEGMENTS.includes(pattern.length)) {
        unique++
      }
    }
  }

  return unique
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const entries = await getInputArray<EntriesConverter>(__dirname, 'entries.txt', converter)

  return calcUniqueSegments(entries).toString()
}

export {
  start
}
