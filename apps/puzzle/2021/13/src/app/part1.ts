import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  PaperConverter,
  Folds,
  Dots,
  Point,
  Points
} from './converter'

/*
 * Calculates if a dot is shown given 2 points
 */
function calcFoldDot (point1: Point, point2: Point): Point {
  if (point1 === '#' || point2 === '#') {
    return '#'
  }

  return '.'
}

/*
 * Calculates fold dots given 2 points
 */
function calcFoldDots (points1: Points, points2: Points): Points {
  for (let r = 0; r < points1.length; r++) {
    for (let c = 0; c < points1[0].length; c++) {
      points1[r][c] = calcFoldDot(points1[r][c], points2[r][c])
    }
  }

  return points1
}

/*
 * Folds points along X axis for position
 */
function foldX (points: Points, position: number): Points {
  const newPoints = [...points.map(p => [...p])]
  const folded: Points = []

  // Fold
  for (let r = 0; r < newPoints.length; r++) {
    const [, ...fold] = newPoints[r].splice(position)
    folded.push(fold.reverse())
  }

  // Square up
  const [big, small] = newPoints[0].length >= folded[0].length ? [newPoints, folded] : [folded, newPoints]
  const diff = big[0].length - small[0].length
  if (diff > 0) {
    for (let r = 0; r < big.length; r++) {
      small[r] = Array(diff).fill([]).concat(small[r])
    }
  }

  return calcFoldDots(big, small)
}

/*
 * Folds points along Y axis for position
 */
function foldY (points: Points, position: number): Points {
  const newPoints = [...points.map(p => [...p])]

  // Fold
  let [, ...folded] = newPoints.splice(position)
  folded = folded.reverse()

  // Square up
  let [big, small] = newPoints.length >= folded.length ? [newPoints, folded] : [folded, newPoints]
  const diff = big.length - small.length
  if (diff > 0) {
    small = Array(diff).fill([]).concat(small)
  }

  return calcFoldDots(big, small)
}

/*
 * Calculates visible dots after first fold
 */
function calcDots (points: Points, folds: Folds): number {
  let newPoints = [...points.map(p => [...p])]
  let dots = 0

  for (const fold of [folds[0]]) {
    if (fold[0] === 'y') {
      newPoints = foldY(newPoints, fold[1])
    } else {
      newPoints = foldX(newPoints, fold[1])
    }
  }

  for (const line of newPoints) {
    for (const point of line) {
      if (point === '#') {
        dots += 1
      }
    }
  }

  return dots
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const paper = await getInputArray<PaperConverter>(__dirname, 'paper.txt', converter)
  let [yMax, xMax] = [0, 0]
  const dots: Dots = []
  const folds: Folds = []

  for (const line of paper) {
    if (Array.isArray(line)) {
      folds.push(line)
    } else {
      dots.push(line)
      if (line.x > xMax) {
        xMax = line.x
      }
      if (line.y > yMax) {
        yMax = line.y
      }
    }
  }

  const points: Points = Array(yMax + 1).fill([]).map(() => Array(xMax + 1).fill('.'))
  for (const dot of dots) {
    points[dot.y][dot.x] = '#'
  }

  return calcDots(points, folds).toString()
}

export {
  start
}
