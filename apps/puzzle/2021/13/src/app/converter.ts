import { Converter } from '@advent-of-code/puzzle/util/core'

export type Axis = 'x' | 'y'
export type Fold = [Axis, number]
export type Folds = Fold[]
export interface Dot {
  x: number
  y: number
}
export type Dots = Dot[]
export type Point = '.' | '#'
export type Points = Point[][]
export type PaperConverter = Converter<Dot | Fold>

/*
 * Converts input
 */
const converter: PaperConverter = line => {
  if (line === '') {
    return null
  } else if (line.startsWith('fold along')) {
    const [axis, position] = line.split('fold along ')[1].split('=')
    return [axis, +position] as Fold
  } else {
    const [x, y] = line.split(',')
    const dot: Dot = { x: +x, y: +y }
    return dot
  }
}

export { converter }
