import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  Binary,
  ReportConverter,
  ReportConverterTypes
} from './converter'

/*
 * Given reports, reduces the reports to those that match the keeper function
 * Keeper function takes the sum of the current col and return what binary to keep
 */
function reduceReports (reports: ReportConverterTypes, c: number, keeper: (sum: number) => Binary): ReportConverterTypes {
  if (reports.length !== 1) {
    let sum = 0

    for (let r = 0; r < reports.length; r++) {
      sum += reports[r][c]
    }

    const keep = keeper(sum)

    return reports.filter(r => r[c] === keep)
  }

  return reports
}

/*
 * Calculates life support rating of submarine
 */
function calcLifeSupportRating (reports: ReportConverterTypes): number {
  let ogrReports = [...reports.map(r => [...r])]
  let c02Reports = [...reports.map(r => [...r])]

  for (let c = 0; c < reports[0].length; c++) {
    if (ogrReports.length === 1 && c02Reports.length === 1) {
      break
    }

    ogrReports = reduceReports(ogrReports, c, sum => sum >= ogrReports.length / 2.0 ? 1 : 0)
    c02Reports = reduceReports(c02Reports, c, sum => sum < c02Reports.length / 2.0 ? 1 : 0)
  }

  return parseInt(ogrReports[0].join(''), 2) * parseInt(c02Reports[0].join(''), 2)
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const reports = await getInputArray<ReportConverter>(__dirname, 'report.txt', converter)

  return calcLifeSupportRating(reports).toString()
}

export { start }
