import { Converter, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export type Binary = 1 | 0
export type ReportConverter = Converter<Binary[]>
export type ReportConverterTypes = ConverterTypes<ReportConverter>

/*
 * Converts input
 */
const converter: ReportConverter = line => {
  return line.split('').map(i => +i as Binary)
}

export { converter }
