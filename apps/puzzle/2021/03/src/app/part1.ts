import { getInputArray } from '@advent-of-code/puzzle/util/core'
import { converter, ReportConverter, ReportConverterTypes } from './converter'

/*
 * Calculates power comsumption of submarine
 */
function calcPowerConsumption (reports: ReportConverterTypes): number {
  const gamma: ReportConverterTypes[0] = []
  const epsilon: ReportConverterTypes[0] = []

  for (let c = 0; c < reports[0].length; c++) {
    let sum = 0

    for (let r = 0; r < reports.length; r++) {
      sum += reports[r][c]
    }

    if (sum > reports.length / 2.0) {
      gamma.push(1)
      epsilon.push(0)
    } else {
      gamma.push(0)
      epsilon.push(1)
    }
  }

  return parseInt(gamma.join(''), 2) * parseInt(epsilon.join(''), 2)
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const reports = await getInputArray<ReportConverter>(__dirname, 'report.txt', converter)

  return calcPowerConsumption(reports).toString()
}

export { start }
