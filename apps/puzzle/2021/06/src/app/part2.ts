import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  Fish,
  FishesConverter,
  FishesConverterType,
  Timers
} from './converter'

// Fresh set of timers all set to 0
const newTimers = { 0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0 }

/*
 * Calculates number of fish after x days
 */
function calcFish (fishes: FishesConverterType, days: number): number {
  let timers: Timers = { ...newTimers }
  for (const fish of fishes) {
    timers[fish] += 1
  }

  for (let d = 0; d < days; d++) {
    const nextTimers: Timers = { ...newTimers }
    for (const [key, num] of Object.entries(timers)) {
      const fish = +key
      if (fish === 0) {
        nextTimers[6] = num
        nextTimers[8] = num
      } else {
        nextTimers[fish - 1 as Fish] += num
      }
    }
    timers = nextTimers
  }

  return Object.values(timers).reduce((t, v) => t + v, 0)
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const [fishes] = await getInputArray<FishesConverter>(__dirname, 'fish.txt', converter)

  return calcFish(fishes, 256).toString()
}

export {
  start
}
