import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  FishesConverter,
  FishesConverterType
} from './converter'

/*
 * Calculates number of fish after x days
 */
function calcFish (fishes: FishesConverterType, days: number): number {
  for (let d = 0; d < days; d++) {
    const numFishes = fishes.length
    for (let f = 0; f < numFishes; f++) {
      if (fishes[f] === 0) {
        fishes[f] = 6
        fishes.push(8)
      } else {
        fishes[f] -= 1
      }
    }
  }

  return fishes.length
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const [fishes] = await getInputArray<FishesConverter>(__dirname, 'fish.txt', converter)

  return calcFish(fishes, 80).toString()
}

export {
  start
}
