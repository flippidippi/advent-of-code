import { Converter, ConverterType } from '@advent-of-code/puzzle/util/core'

export type Fish = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8
export type FishesConverter = Converter<Fish[]>
export type FishesConverterType = NonNullable<ConverterType<FishesConverter>>
export type Timers = { [k in Fish]: number }

/*
 * Converts input
 */
const converter: FishesConverter = line => {
  return line.split(',').map(c => +c as Fish)
}

export { converter }
