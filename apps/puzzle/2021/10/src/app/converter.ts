import { Converter, ConverterType, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export type CharacterOpen = '(' | '[' | '{' | '<'
export type CharacterClose = ')' | ']' | '}' | '>'
export type Character = CharacterOpen | CharacterClose
export type LinesConverter = Converter<Character[]>
export type LinesConverterType = NonNullable<ConverterType<LinesConverter>>
export type LinesConverterTypes = ConverterTypes<LinesConverter>
export const characterMap: { [k in CharacterOpen]: CharacterClose } = {
  '(': ')',
  '[': ']',
  '{': '}',
  '<': '>'
}
export const charactersOpen = Object.keys(characterMap) as Character[]
export const charactersClose = Object.values(characterMap) as Character[]
export const characterScores: { [k in CharacterClose]: number } = {
  ')': 3,
  ']': 57,
  '}': 1197,
  '>': 25137
}
export const characterPoints: { [k in CharacterClose]: number } = {
  ')': 1,
  ']': 2,
  '}': 3,
  '>': 4
}

/*
 * Converts input
 */
const converter: LinesConverter = line => {
  return line.split('') as Character[]
}

export { converter }
