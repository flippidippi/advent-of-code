import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  LinesConverter,
  LinesConverterType,
  LinesConverterTypes,
  Character,
  CharacterOpen,
  CharacterClose,
  characterMap,
  charactersOpen,
  characterScores
} from './converter'

/*
 * Calculates the line's error score
 */
function calcLineErrorScore (line: LinesConverterType): number {
  const characters: LinesConverterType = []

  for (const character of line) {
    if (charactersOpen.includes(character)) {
      characters.push(character)
    } else {
      const popped: Character | undefined = characters.pop()
      if (popped != null && character !== characterMap[popped as CharacterOpen]) {
        return characterScores[character as CharacterClose]
      }
    }
  }

  return 0
}

/*
 * Calculates the total sytax error score
 */
function calcSyntaxErrorScore (lines: LinesConverterTypes): number {
  return lines.reduce((t, l) => t + calcLineErrorScore(l), 0)
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const lines = await getInputArray<LinesConverter>(__dirname, 'lines.txt', converter)

  return calcSyntaxErrorScore(lines).toString()
}

export {
  start
}
