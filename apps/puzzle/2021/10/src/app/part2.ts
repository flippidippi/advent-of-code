import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  LinesConverter,
  LinesConverterType,
  LinesConverterTypes,
  Character,
  CharacterOpen,
  characterMap,
  charactersOpen,
  characterPoints
} from './converter'

/*
 * Calculates the score for completion strings of incomplete lines
 * Returns null if the line is not an incomplete line
 */
function calcCompletionScore (line: LinesConverterType): number | null {
  const characters: LinesConverterType = []
  let completionScore = 0

  for (const character of line) {
    if (charactersOpen.includes(character)) {
      characters.push(character)
    } else {
      const popped: Character | undefined = characters.pop()
      if (popped != null && character !== characterMap[popped as CharacterOpen]) {
        return null
      }
    }
  }

  while (characters.length > 0) {
    const popped: Character | undefined = characters.pop()
    if (popped != null) {
      completionScore = completionScore * 5 + characterPoints[characterMap[popped as CharacterOpen]]
    }
  }

  return completionScore
}

/*
 * Calculates the middle score for completion strings of incomplete lines
 */
function calcMiddleCompletionScore (lines: LinesConverterTypes): number {
  const scores = []

  for (const line of lines) {
    const score = calcCompletionScore(line)
    if (score != null) {
      scores.push(score)
    }
  }

  return scores.sort((a, b) => a - b)[Math.floor(scores.length / 2)]
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const lines = await getInputArray<LinesConverter>(__dirname, 'lines.txt', converter)

  return calcMiddleCompletionScore(lines).toString()
}

export {
  start
}
