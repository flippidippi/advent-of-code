import { start as startPart1 } from './app/part1'
import { start as startPart2 } from './app/part2'
import { start } from '@advent-of-code/puzzle/util/core'

start('2021', '10', startPart1, startPart2).catch(console.error)
