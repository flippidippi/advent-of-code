import { Converter, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export type BingoConverter = Converter<number[]>
export type BingoConverterTypes = ConverterTypes<BingoConverter>
export type Draws = number[]
export type Board = number[][]
export type Boards = Board[]
export type BoardPlaying = Array<Array<number | null>>
export type BoardsPlaying = BoardPlaying[]

/*
 * Converts input
 */
const converter: BingoConverter = line => {
  if (line.includes(',')) {
    return line.split(',').map(i => +i)
  } else if (line.length > 0) {
    return line.split(' ').filter(n => n !== '').map(i => +i)
  } else {
    return null
  }
}

/*
 * Converts board lines into array of boards
 */
function convertBoardLines (boardLines: BingoConverterTypes): Boards {
  const boards: Boards = []

  for (const [index, boardLine] of boardLines.entries()) {
    const boardNum = Math.floor(index / 5)
    if (boards[boardNum] == null) {
      boards[boardNum] = []
    }
    boards[boardNum].push(boardLine)
  }

  return boards
}

export { converter, convertBoardLines }
