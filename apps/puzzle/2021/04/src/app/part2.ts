import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  convertBoardLines,
  BingoConverter,
  Draws,
  Boards,
  BoardPlaying,
  BoardsPlaying
} from './converter'

/*
 * Calculates the board score
 */
function calcBoardScore (board: BoardPlaying): number {
  let score = 0

  for (const r of board) {
    for (const c of r) {
      if (c != null) {
        score += c
      }
    }
  }

  return score
}

/*
 * Gets board score if winner
 */
function getBoardScore (board: BoardPlaying): number | null {
  // Horizontal check
  for (const r of board) {
    if (r.every(c => c == null)) {
      return calcBoardScore(board)
    }
  }

  // Vertical check
  for (let c = 0; c < 5; c++) {
    for (let r = 0; r < 5; r++) {
      if (board[r][c] !== null) {
        break
      } else if (r === 4) {
        return calcBoardScore(board)
      }
    }
  }

  return null
}

/*
 * Marks boards and checks for a new winner score, if no winner, returns new boards marked
 */
function calcDraw (
  draw: number,
  boards: BoardsPlaying
): [BoardsPlaying, number | null] {
  for (const [index, board] of boards.entries()) {
    for (let r = 0; r < 5; r++) {
      for (let c = 0; c < 5; c++) {
        if (board[r][c] === draw) {
          board[r][c] = null
        }
      }
    }

    const boardScore = getBoardScore(board)
    if (boardScore != null) {
      if (boards.length === 1) {
        return [boards, boardScore]
      }
      boards[index] = []
    }
  }

  return [boards.filter(b => b.length > 1), null]
}

/*
 * Calculates the final bingo score
 */
function calcBingoScore (draws: Draws, boards: Boards): number {
  let boardsPlaying: BoardsPlaying = [...boards.map(b => [...b.map(r => [...r])])]

  for (const draw of draws) {
    const [newBoardPlaying, boardScore] = calcDraw(draw, boardsPlaying)

    if (boardScore != null) {
      return boardScore * draw
    }
    boardsPlaying = newBoardPlaying
  }

  return 0
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const [draws, ...boardLines] = await getInputArray<BingoConverter>(__dirname, 'bingo.txt', converter)

  return calcBingoScore(draws, convertBoardLines(boardLines)).toString()
}

export { start }
