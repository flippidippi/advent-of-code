import { Converter } from '@advent-of-code/puzzle/util/core'

export type Cave = string
export type Caves = Cave[]
export type Path = Caves
export type Paths = Path[]
export type Connection = [Cave, Cave]
export type ConnectionsConverter = Converter<Connection>
export type ConnectionMap = { [k in Cave]: Cave[] }

/*
 * Converts input
 */
const converter: ConnectionsConverter = line => {
  return line.split('-') as Connection
}

export { converter }
