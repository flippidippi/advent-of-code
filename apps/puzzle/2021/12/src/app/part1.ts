import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  ConnectionsConverter,
  Cave,
  Caves,
  ConnectionMap,
  Path,
  Paths
} from './converter'

/*
 * Given the current path, calculates the paths to end
 */
function calcPath (path: Path, caves: Caves, map: ConnectionMap): Paths {
  const cave: Cave = path[path.length - 1]
  const newCaves = [...caves]
  let paths: Paths = []

  if (cave.toLowerCase() === cave) {
    newCaves.splice(caves.indexOf(cave), 1)
  }

  for (const newCave of map[cave]) {
    if (newCave === 'end') {
      paths.push([...path, newCave])
    } else if (newCaves.includes(newCave)) {
      paths = paths.concat(calcPath([...path, newCave], newCaves, map))
    }
  }

  return paths
}

/*
 * Calculates number of paths through cave system that visit small caves at most once
 */
function calcPaths (caves: Caves, map: ConnectionMap): number {
  return calcPath(['start'] as Path, caves, map).length
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const connections = await getInputArray<ConnectionsConverter>(__dirname, 'connections.txt', converter)
  const caves = new Set<Cave>()
  const map: ConnectionMap = {}

  for (const [cave1, cave2] of connections) {
    caves.add(cave1)
    caves.add(cave2)
    map[cave1] = map[cave1] ?? []
    map[cave2] = map[cave2] ?? []
    if (cave2 !== 'start' && cave1 !== 'end') {
      map[cave1].push(cave2)
    }
    if (cave1 !== 'start' && cave2 !== 'end') {
      map[cave2].push(cave1)
    }
  }

  return calcPaths([...caves], map).toString()
}

export {
  start
}
