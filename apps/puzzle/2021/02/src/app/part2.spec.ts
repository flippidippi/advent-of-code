import { start } from './part2'

describe('part 2', () => {
  it('should have the correct answer', async () => {
    const result = await start()
    expect(result).toEqual('2101031224')
  })
})
