import { start } from './part1'

describe('part 1', () => {
  it('should have the correct answer', async () => {
    const result = await start()
    expect(result).toEqual('2102357')
  })
})
