import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  CommandConverter,
  CommandConverterTypes
} from './converter'

/*
 * Calculates horizontal position and depth multiplied together
 */
function calcPosition (commands: CommandConverterTypes): number {
  let horizontal = 0
  let depth = 0
  let aim = 0

  for (const [command, value] of commands) {
    switch (command) {
      case 'forward':
        horizontal += value
        depth += aim * value
        break
      case 'down':
        aim += value
        break
      case 'up':
        aim -= value
        break
    }
  }

  return horizontal * depth
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const commands = await getInputArray<CommandConverter>(__dirname, 'commands.txt', converter)

  return calcPosition(commands).toString()
}

export { start }
