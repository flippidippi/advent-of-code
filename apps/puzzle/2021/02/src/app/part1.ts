import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  CommandConverter,
  CommandConverterTypes
} from './converter'

/*
 * Calculates horizontal position and depth multiplied together
 */
function calcPosition (commands: CommandConverterTypes): number {
  let horizontal = 0
  let depth = 0

  for (const [command, value] of commands) {
    switch (command) {
      case 'forward':
        horizontal += value
        break
      case 'down':
        depth += value
        break
      case 'up':
        depth -= value
        break
    }
  }

  return horizontal * depth
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const commands = await getInputArray<CommandConverter>(__dirname, 'commands.txt', converter)

  return calcPosition(commands).toString()
}

export { start }
