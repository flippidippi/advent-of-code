import { Converter, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export type Command = 'forward' | 'down' | 'up'
export type CommandConverter = Converter<[Command, number]>
export type CommandConverterTypes = ConverterTypes<CommandConverter>

/*
 * Converts input
 */
const converter: CommandConverter = line => {
  const [command, amount] = line.split(' ')

  return [command as Command, +amount]
}

export { converter }
