import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  PolymerConverter,
  PolymerTemplate,
  PolymerInsertions,
  PolymerInsertionMap,
  ElementMap
} from './converter'

/*
 * Calculates the new template for a single insertion
 */
function calcInsertion (elementMap: ElementMap, newElementMap: ElementMap, insertionMap: PolymerInsertionMap): ElementMap {
  for (const [element, count] of Object.entries(elementMap)) {
    if (count > 0) {
      const insert = insertionMap[element]
      newElementMap[`${element[0]}${insert}`] += count
      newElementMap[`${insert}${element[1]}`] += count
    }
  }

  return newElementMap
}

/*
 * Calculates polymer diff between the most common and least common elements
 */
function calcPolymerDiff (template: PolymerTemplate, insertionMap: PolymerInsertionMap, steps: number): number {
  // New element map zeroed out
  const newElementMap: ElementMap = {}
  for (const element of Object.keys(insertionMap)) {
    newElementMap[element] = 0
  }

  // Prep element map with inital template
  let elementMap: ElementMap = { ...newElementMap }
  for (let i = 0; i < template.length - 1; i += 1) {
    const element = `${template[i]}${template[i + 1]}`
    elementMap[element] += 1
  }

  // Calculate steps
  for (let i = 0; i < steps; i++) {
    elementMap = calcInsertion(elementMap, { ...newElementMap }, insertionMap)
  }

  // Calculate totals
  const countMap: ElementMap = {}
  for (const element of Object.keys(insertionMap)) {
    countMap[element[0]] = 0
  }
  countMap[template[template.length - 1]] += 1
  for (const [element, count] of Object.entries(elementMap)) {
    countMap[element[0]] += count
  }
  const values = Object.values(countMap)

  return Math.max(...values) - Math.min(...values)
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const [template, ...insertions] = await getInputArray<PolymerConverter>(__dirname, 'polymer.txt', converter) as [PolymerTemplate, ...PolymerInsertions]
  const insertionMap: PolymerInsertionMap = {}

  for (const insertion of insertions) {
    insertionMap[insertion.start] = insertion.end
  }

  return calcPolymerDiff(template, insertionMap, 40).toString()
}

export {
  start
}
