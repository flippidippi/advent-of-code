import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  PolymerConverter,
  PolymerTemplate,
  PolymerInsertions,
  PolymerInsertionMap,
  ElementMap
} from './converter'

/*
 * Calculates the new template for a single insertion
 */
function calcInsertion (template: PolymerTemplate, insertionMap: PolymerInsertionMap): PolymerTemplate {
  const newTemplate: PolymerTemplate = [...template]

  for (let i = 0; i < newTemplate.length - 1; i += 2) {
    newTemplate.splice(i + 1, 0, insertionMap[`${newTemplate[i]}${newTemplate[i + 1]}`])
  }

  return newTemplate
}

/*
 * Calculates polymer diff between the most common and least common elements
 */
function calcPolymerDiff (template: PolymerTemplate, insertionMap: PolymerInsertionMap, steps: number): number {
  let newTemplate = [...template]

  for (let i = 0; i < steps; i++) {
    newTemplate = calcInsertion(newTemplate, insertionMap)
  }

  const elementMap: ElementMap = {}
  for (const element of newTemplate) {
    elementMap[element] = elementMap[element] != null ? elementMap[element] + 1 : 1
  }
  const values = Object.values(elementMap)

  return Math.max(...values) - Math.min(...values)
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const [template, ...insertions] = await getInputArray<PolymerConverter>(__dirname, 'polymer.txt', converter) as [PolymerTemplate, ...PolymerInsertions]
  const insertionMap: PolymerInsertionMap = {}

  for (const insertion of insertions) {
    insertionMap[insertion.start] = insertion.end
  }

  return calcPolymerDiff(template, insertionMap, 10).toString()
}

export {
  start
}
