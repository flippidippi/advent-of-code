import { Converter } from '@advent-of-code/puzzle/util/core'

export interface PolymerInsertion {
  start: string
  end: string
}
export type PolymerInsertions = PolymerInsertion[]
export type PolymerTemplate = string[]
export interface PolymerInsertionMap { [k: string]: string }
export type PolymerConverter = Converter<PolymerTemplate | PolymerInsertion>
export interface ElementMap { [k: string]: number }

/*
 * Converts input
 */
const converter: PolymerConverter = line => {
  if (line === '') {
    return null
  } else if (line.includes('->')) {
    const [start, end] = line.split(' -> ')
    const insertion: PolymerInsertion = { start, end }
    return insertion
  } else {
    return line.split('') as PolymerTemplate
  }
}

export { converter }
