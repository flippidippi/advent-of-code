import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  LineConverter,
  LineConverterTypes,
  Line,
  Points
} from './converter'

/*
 * Add points from line
 */
function addPoints (points: Points, line: Line): void {
  let [x, y] = [line.x1, line.y1]
  const [xSign, ySign] = [Math.sign(line.x2 - line.x1), Math.sign(line.y2 - line.y1)]
  const [xEnd, yEnd] = [line.x2 + xSign, line.y2 + ySign]

  while (x !== xEnd || y !== yEnd) {
    const point = `${x},${y}`
    points[point] = (points[point] ?? 0) + 1
    x += xSign
    y += ySign
  }
}

/*
 * Calculates number of points that overlap
 */
function calcOverlaps (lines: LineConverterTypes): number {
  const points: Points = {}

  for (const line of lines) {
    addPoints(points, line)
  }

  return Object.values(points).filter(v => v >= 2).length
}

/*
 * Part 2 implementation
 */
async function start (): Promise<string> {
  const lines = await getInputArray<LineConverter>(__dirname, 'lines.txt', converter)

  return calcOverlaps(lines).toString()
}

export { start }
