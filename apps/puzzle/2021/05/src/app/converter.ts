import { Converter, ConverterTypes } from '@advent-of-code/puzzle/util/core'

export interface Line {
  x1: number
  y1: number
  x2: number
  y2: number
}
export type LineConverter = Converter<Line>
export type LineConverterTypes = ConverterTypes<LineConverter>
export interface Points {
  [k: string]: number
}

/*
 * Converts input
 */
const converter: LineConverter = line => {
  const [x1, y1, x2, y2] = line.replace(' -> ', ',').split(',').map(c => +c)

  return { x1, y1, x2, y2 }
}

export { converter }
