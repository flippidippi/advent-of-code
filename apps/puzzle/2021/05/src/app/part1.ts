import { getInputArray } from '@advent-of-code/puzzle/util/core'
import {
  converter,
  LineConverter,
  LineConverterTypes,
  Points
} from './converter'

/*
 * Add points from line
 */
function addPoints (points: Points, start: number, end: number, point: (p: number) => string): void {
  const sign = Math.sign(end - start)
  for (let i = start; i !== end + sign; i = i + sign) {
    const p = point(i)
    points[p] = (points[p] ?? 0) + 1
  }
}

/*
 * Calculates number of points that overlap
 */
function calcOverlaps (lines: LineConverterTypes): number {
  const points: Points = {}

  for (const line of lines) {
    if (line.x1 === line.x2) {
      addPoints(points, line.y1, line.y2, p => `${line.x1},${p}`)
    } else if (line.y1 === line.y2) {
      addPoints(points, line.x1, line.x2, p => `${p},${line.y1}`)
    }
  }

  return Object.values(points).filter(v => v >= 2).length
}

/*
 * Part 1 implementation
 */
async function start (): Promise<string> {
  const lines = await getInputArray<LineConverter>(__dirname, 'lines.txt', converter)

  return calcOverlaps(lines).toString()
}

export { start }
